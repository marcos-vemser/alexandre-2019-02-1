public class Elfo{
    private String nome;
    private Inventario itens = new Inventario();
    private int experiencia=0;
    private int indiceFlecha = 0;

    public Elfo(String nome){
        this.nome=nome;
        itens.adicionarItem(new Item(2,"Flecha"));  
        itens.adicionarItem(new Item(1,"Arco"));
    }
    
    public String getNome(){
        return this.nome;
    }

    public void setNome(String nome){
        this.nome=nome;
    }

    public int getExperiencia(){
        return this.experiencia;
    }

    public Item getFlecha(){
        return this.itens.obterItem(indiceFlecha);
    }

    public int getQtdFlecha(){
        return this.itens.obterItem(indiceFlecha).getQuantidade();
    }

    public boolean podeAtirarFlecha(){
        return this.itens.obterItem(indiceFlecha).getQuantidade()>0;
    } 

    public void atirarFlechaNoAnao(Dwarf dwarf){
        int qtdAtual = this.itens.obterItem(indiceFlecha).getQuantidade();
        if(podeAtirarFlecha()){
            this.itens.obterItem(indiceFlecha).setQuantidade(--qtdAtual);
            this.experiencia = experiencia + 1;
            dwarf.sofreDano();
        }
    }
}

