
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DwarfTest
{
    @Test
    public void dwarfNasceCom110DeVida(){
        Dwarf dwarf = new Dwarf("Dain");
        assertEquals(110.0, dwarf.getVida(), .000001 /* ou 1e-9 */);
    }

    @Test
    public void dwarfNasceCom110Vida(){
        Dwarf dwarf = new Dwarf("Dain");
        dwarf.sofreDano();
        assertEquals (100.0, dwarf.getVida(), .000001);
    }

    @Test
    public void dwarfPerdeTodaVida11Ataques(){
        Dwarf dwarf = new Dwarf("Dain");
        for(int i = 0; i < 11; i++){
            dwarf.sofreDano();
        }
        assertEquals(0.0, dwarf.getVida(), 1e-9);
    }
}
