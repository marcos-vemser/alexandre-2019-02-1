import java.util.ArrayList;

public class Inventario {
    private ArrayList<Item> itens = new ArrayList<Item>();

    public ArrayList<Item> getItens() {
        return itens;
    }

    public void adicionarItem(Item item) {
        this.itens.add(item);
    }

    public void removerItem(int posicao) {
        this.itens.remove(posicao);
    }

    public Item obterItem(int posicao) {
        return this.itens.get(posicao);
    }

    public String retornarListaNomeItem() {
        StringBuilder descricoes = new StringBuilder();

        for (int i = 0; i < this.itens.size(); i++) {
            Item item = this.itens.get(i);
            if (item != null) {
                descricoes.append(item.getDescricao());
                descricoes.append(",");
            }
        }
        return (descricoes.length() > 0 ? descricoes.substring(0, (descricoes.length() - 1)) : descricoes.toString());
    }

    public Item retornaItemComMaiorQtd() {
        int indice = 0, maiorQuantidade = 0;
        for (int i = 0; i < this.itens.size(); i++) {
            Item item = this.itens.get(i);

            if (itens.get(i).getQuantidade() > maiorQuantidade) {

                maiorQuantidade = item.getQuantidade();
                indice = i;
            }
        }

        return this.itens.size() > 0 ? this.itens.get(indice) : null;
    }

    public Item buscar(String nome) {
        int aux = 0;
        for (int i = 0; i < this.itens.size(); i++) {
            if (this.itens.get(i).getDescricao().equals(nome)) {
                aux = i;
            }
        }
        return itens.get(aux);
    }

    public String retornarListaNomeInvertida() {
        StringBuilder descricoes = new StringBuilder();
        int aux = itens.size()-1;
        for (int i = aux; i >= 0; i--) {
            Item item = this.itens.get(i);
            if (item != null) {
                descricoes.append(item.getDescricao());
                descricoes.append(",");
            }
        }
        return (descricoes.length() > 0 ? descricoes.substring(0, (descricoes.length() - 1)) : descricoes.toString());
    }

    public String ordenarItens() {
         int aux = itens.size()-1;
        
        while (aux>0){
            int last = 0;
            for(int i = 0; i < aux; i++){
                if(itens.get(i).getQuantidade() > itens.get(i+1).getQuantidade()){
                    Item item = itens.get(i);
                    itens.set(i, itens.get(i+1));
                    itens.set((i+1), item);
                    last = i;
                }
            }
            aux = last;
        }
        
        String listaOrdenada = " "+itens.get(0).getQuantidade();
        int i = 1;
        while (i<itens.size()){
            listaOrdenada+= ", "+itens.get(i).getQuantidade();
            i++;
        }
        
        return listaOrdenada;
    }

    public String ordenarItensDesc() {
        int aux = itens.size()-1;
        
        while (aux>0){
            int ultimo = 0;
            for(int i = 0; i < aux; i++){
                if(itens.get(i).getQuantidade() < itens.get(i+1).getQuantidade()){
                    Item itemAux = itens.get(i);
                    itens.set(i, itens.get(i+1));
                    itens.set((i+1), itemAux);
                    ultimo = i;
                }
            }
            aux = ultimo;
        }
        
        String s = " "+itens.get(0).getQuantidade();
        int i = 1;
        while (i<itens.size()){
            s+= ", "+itens.get(i).getQuantidade();
            i++;
        }
        
        return s;
    }

     public String ordenarItens(TipoOrdenacao tipo){
        if(tipo == TipoOrdenacao.ASC){
            return this.ordenarItens();
        }
        else
            return this.ordenarItensDesc();
    }

}
