public class PaginadorInventario {
    private Inventario inventario;
    public PaginadorInventario(Inventario inventario){
        this.inventario = inventario;
    }       

    public String paginador(int pular, int limitar){
        StringBuilder descricoes = new StringBuilder(); 

        for(int i = pular; i <(pular+limitar) ; i++){ 
            Item item = this.inventario.getItens().get(i);

            descricoes.append("("+item.getQuantidade()+","+item.getDescricao()+");");
        }

        return (descricoes.length() > 0 ? descricoes.substring(0, (descricoes.length()-1)) : descricoes.toString());
    }   
}
