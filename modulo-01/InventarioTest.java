
import java.util.*;
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class InventarioTest
{   
    @Test
    public void adicionarDoisItens(){
        Inventario inventario = new Inventario();
        Item espada = new Item(1,"espada");
        Item escudo = new Item(2,"escudo");
        
        inventario.adicionarItem(espada);
        inventario.adicionarItem(escudo);
        
        assertEquals(espada, inventario.getItens().get(0));
        assertEquals(escudo, inventario.getItens().get(1));
    }
   
     @Test
    public void ObterItemAdicionado(){
        Inventario inventario = new Inventario();
        Item espada = new Item(1,"espada");
        
        inventario.adicionarItem(espada);
        
        assertEquals(espada, inventario.getItens().get(0));
        
    }
    
    @Test
    public void renoverItemAntesDeAdicionarProximo(){
        Inventario inventario = new Inventario();
        
        Item espada = new Item(1,"espada");
        Item escudo = new Item(2,"escudo");
        
        inventario.adicionarItem(espada);
        inventario.removerItem(0);
        inventario.adicionarItem(escudo);
        assertEquals(escudo, inventario.getItens().get(0));
        assertEquals(1, inventario.getItens().size());
    }
    
    @Test
    public void getDescricoesVariosItens(){
        Inventario inventario = new Inventario();
        
        Item espada = new Item(1,"espada");
        Item escudo = new Item(2,"escudo");
        Item armadura = new Item(1,"armadura");
        
        inventario.adicionarItem(espada);
        inventario.adicionarItem(armadura);
        inventario.adicionarItem(escudo);
        
        assertEquals("espada,armadura,escudo", inventario.retornarListaNomeItem());
       
    }
    
    @Test
    public void getDescricoesNenhumItem() {
        Inventario inventario = new Inventario();
        
        assertEquals("", inventario.retornarListaNomeItem());
    }
    
    @Test
    public void getItemMaiorQuantidadeComVarios(){
        Inventario inventario = new Inventario();
        
        Item espada = new Item(1,"espada");
        Item escudo = new Item(2,"escudo");
        Item armadura = new Item(1,"armadura");
        
        inventario.adicionarItem(espada);
        inventario.adicionarItem(armadura);
        inventario.adicionarItem(escudo);
        
        assertEquals(escudo, inventario.retornaItemComMaiorQtd());
    }
    
    @Test
    public void getItemMaiorQuantidadeComInventarioVazio(){
        Inventario inventario = new Inventario();
        assertNull(inventario.retornaItemComMaiorQtd());
    }
    
    @Test
    public void getItemMaiorQuantidadeComItensMesmaQuantidade(){
        Inventario inventario = new Inventario();
        
        Item espada = new Item(1,"espada");
        Item escudo = new Item(1,"escudo");
        Item armadura = new Item(1,"armadura");
        
        inventario.adicionarItem(espada);
        inventario.adicionarItem(armadura);
        inventario.adicionarItem(escudo);
        
        assertEquals(espada, inventario.retornaItemComMaiorQtd());
    }
    
    @Test
    public void acharNomeDeItem(){
        Inventario inventario = new Inventario();
        
        Item espada = new Item(1,"espada");
        Item escudo = new Item(1,"escudo");
        Item armadura = new Item(1,"armadura");
        
        inventario.adicionarItem(espada);
        inventario.adicionarItem(armadura);
        inventario.adicionarItem(escudo);
        
        assertEquals(espada, inventario.buscar(espada.getDescricao()));
    }
    
    @Test
    public void exibirListaInvertida(){
        Inventario inventario = new Inventario();
        
        Item espada = new Item(1,"espada");
        Item escudo = new Item(1,"escudo");
        Item armadura = new Item(1,"armadura");
        
        inventario.adicionarItem(espada);
        inventario.adicionarItem(armadura);
        inventario.adicionarItem(escudo);
        
        assertEquals("escudo,armadura,espada", inventario.retornarListaNomeInvertida());
    }
    
    @Test
    public void testarMetodoOrdenacao(){
        Inventario inventario = new Inventario();
        
        Item bracelete = new Item(8,"bracelete");
        Item espada = new Item(3,"espada");
        Item escudo = new Item(2,"escudo");
        Item armadura = new Item(1,"armadura");
        Item flecha = new Item(4,"flecha");
        
        inventario.adicionarItem(bracelete);
        inventario.adicionarItem(espada);
        inventario.adicionarItem(armadura);
        inventario.adicionarItem(escudo);
        inventario.adicionarItem(flecha);
        
       assertEquals(" 1, 2, 3, 4, 8", inventario.ordenarItens());
    }
    
    @Test
    public void testarMetodoOrdenacaoDesc(){
        Inventario inventario = new Inventario();
        
        Item bracelete = new Item(8,"bracelete");
        Item espada = new Item(3,"espada");
        Item escudo = new Item(2,"escudo");
        Item armadura = new Item(1,"armadura");
        Item flecha = new Item(4,"flecha");
        
        inventario.adicionarItem(bracelete);
        inventario.adicionarItem(espada);
        inventario.adicionarItem(armadura);
        inventario.adicionarItem(escudo);
        inventario.adicionarItem(flecha);
        
        assertEquals(" 8, 4, 3, 2, 1", inventario.ordenarItensDesc());
    }
    
    @Test
    public void testarMetodoOrdenacaoTipoOrdenacaoAsc(){
        Inventario inventario = new Inventario();
        
        Item bracelete = new Item(8,"bracelete");
        Item espada = new Item(3,"espada");
        Item escudo = new Item(2,"escudo");
        Item armadura = new Item(1,"armadura");
        Item flecha = new Item(4,"flecha");
        
        inventario.adicionarItem(bracelete);
        inventario.adicionarItem(espada);
        inventario.adicionarItem(armadura);
        inventario.adicionarItem(escudo);
        inventario.adicionarItem(flecha);
        
        assertEquals(" 1, 2, 3, 4, 8", inventario.ordenarItens(TipoOrdenacao.ASC));
    }
    
    @Test
    public void testarMetodoOrdenacaoTipoOrdenacaoDesc(){
        Inventario inventario = new Inventario();
        
        Item bracelete = new Item(8,"bracelete");
        Item espada = new Item(3,"espada");
        Item escudo = new Item(2,"escudo");
        Item armadura = new Item(1,"armadura");
        Item flecha = new Item(4,"flecha");
        
        inventario.adicionarItem(bracelete);
        inventario.adicionarItem(espada);
        inventario.adicionarItem(armadura);
        inventario.adicionarItem(escudo);
        inventario.adicionarItem(flecha);
        
       assertEquals(" 8, 4, 3, 2, 1", inventario.ordenarItens(TipoOrdenacao.DESC));
    }
}
