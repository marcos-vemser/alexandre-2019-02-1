
public class Dwarf {
	private String nome;
	private double vida= 110.0;
	private Status status = Status.RECEM_CRIADO;
	
	public Dwarf(String nome) {
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public double getVida() {
		return vida;
	}
	
	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}
	
	
	public boolean podeSofrerDano() {
		return this.vida>0;
	}
	
	public double mataDwarf() {
		if(this.vida==0.0) {
			this.setStatus(Status.MORTO);
		}
		return 0.0;
	}
	
	public void sofreDano() {
		if(podeSofrerDano()) {
			this.vida = this.vida>10 ? this.vida-10.0 : mataDwarf();
		}	
	}

}
