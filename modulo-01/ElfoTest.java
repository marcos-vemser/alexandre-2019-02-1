
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoTest
{
    @Test
    public void atirarFlechaDiminuirFlechaAumentarXP(){
        Dwarf novoAnao = new Dwarf("Dain");
        Elfo novoElfo = new Elfo("Legolas");
        novoElfo.atirarFlechaNoAnao(novoAnao);
        assertEquals(1,novoElfo.getExperiencia());
        assertEquals(1,novoElfo.getQtdFlecha());
        assertEquals(100.0, novoAnao.getVida(),.00001);
    } 

    @Test
    public void atirarFlecha3VezesDevePerderFlechaAumentarXp(){
        Elfo novoElfo = new Elfo("Legolas");
        Dwarf novoAnao = new Dwarf("Mulungrid");
        novoElfo.atirarFlechaNoAnao(novoAnao);
        novoElfo.atirarFlechaNoAnao(novoAnao);
        novoElfo.atirarFlechaNoAnao(novoAnao);
        assertEquals(2, novoElfo.getExperiencia());
        assertEquals(0, novoElfo.getQtdFlecha());
    }

    @Test
    public void elfosNascemCom2Flechas(){
        Elfo novoElfo = new Elfo("Legolas");
        assertEquals(2, novoElfo.getQtdFlecha());
    }
}
